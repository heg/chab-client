export default (context) => () => ({
  cget() {
    const Api = context.app.$api
    return Api.get(`/public/prestataires`)
  },
})
