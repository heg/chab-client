export default (context) => () => ({
  getStateChanges(startDate, endDate, user = null, presta = null) {
    const Api = context.app.$api
    return Api.get(
      `/stats/tickets/changes?start_date=${startDate}&end_date=${endDate}&user=${
        user || ''
      }&prestataire=${presta || ''}`
    )
  },
})
