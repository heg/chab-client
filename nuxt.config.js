export default {
  publicRuntimeConfig: {
    version: process.env.npm_package_version,
  },
  server: {
    port: process.env.SERVER_PORT || 3001, // par défaut: 3001
    host: process.env.SERVER_HOST || '0.0.0.0', // par défaut: localhost
  },
  // Disable server-side rendering: https://go.nuxtjs.dev/ssr-mode
  ssr: false,

  // Target: https://go.nuxtjs.dev/config-target
  target: 'static',

  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'chab-client',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      {
        rel: 'stylesheet',
        href: 'https://fonts.googleapis.com/css2?family=Cousine&Raleway&display=swap',
      },
    ],
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [],

  router: {
    extendRoutes(routes, resolve) {
      routes.push(
        {
          path: '/login',
          component: 'pages/login.vue',
        },
        {
          path: '/reports',
          component: 'pages/reports/index.vue',
        },
        {
          path: '/reports/state_changes',
          component: 'pages/reports/state_changes.vue',
        },
        {
          path: '/bc',
          component: 'pages/index-bc.vue',
          props: {
            role: 'bc',
          },
        },
        {
          path: '/comptoir',
          component: 'pages/index-comptoir.vue',
          props: {
            role: 'comptoir',
          },
        },
        {
          path: '/:role/verifier',
          component: 'pages/billets/verifier.vue',
          props: true,
        },
        {
          path: '/:role/emission',
          component: 'pages/billets/emission.vue',
          props: true,
        },
        {
          path: '/:role/change',
          component: 'pages/billets/change.vue',
          props: true,
        },
        {
          path: '/:role/monnaie',
          component: 'pages/billets/monnaie.vue',
          props: true,
        },
        {
          path: '/:role/replace',
          component: 'pages/billets/replace.vue',
          props: true,
        },
        {
          path: '/bc/verifier',
          component: 'pages/billets/verifier.vue',
        },
        {
          path: '/bc/emission',
          component: 'pages/billets/emission.vue',
        },
        {
          path: '/bc/change',
          component: 'pages/billets/change.vue',
        },
        {
          path: '/bc/monnaie',
          component: 'pages/billets/monnaie.vue',
        },
        {
          path: '/bc/replace',
          component: 'pages/billets/replace.vue',
        },
        {
          path: '/comptoir/verifier',
          component: 'pages/billets/verifier.vue',
        },
        {
          path: '/comptoir/emission',
          component: 'pages/billets/emission.vue',
        },
        {
          path: '/comptoir/change',
          component: 'pages/billets/change.vue',
        },
        {
          path: '/comptoir/monnaie',
          component: 'pages/billets/monnaie.vue',
        },
        {
          path: '/comptoir/replace',
          component: 'pages/billets/replace.vue',
        }
      )
    },
    trailingSlash: true,
  },

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    '~/plugins/event-bus',
    '~/plugins/Api.js',
    '~/plugins/Models.js',
    // '~/node_modules/html5-qrcode/minified/html5-qrcode.min.js',
    '~/plugins/QRCode.js',
    '~/plugins/VCalendar.js',
    '~/plugins/VSelect.js',
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/eslint
    ['@nuxtjs/eslint-module', { fix: true }],
    // https://go.nuxtjs.dev/stylelint
    ['@nuxtjs/stylelint-module', { fix: true }],
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    // https://go.nuxtjs.dev/pwa
    '@nuxtjs/pwa',
  ],

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {},

  // PWA module configuration: https://go.nuxtjs.dev/pwa
  pwa: {
    manifest: {
      lang: 'en',
    },
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {},
}
