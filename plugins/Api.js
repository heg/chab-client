import axios from 'axios'

export default (context, inject) => {
  let basePath = '/api'
  if (context.isDev) {
    basePath = `http://192.168.1.15:8080`
  }

  const api = {
    get: (endpoint) => {
      return new Promise((resolve, reject) => {
        axios
          .get(`${basePath}${endpoint}`)
          .then(function (data) {
            return resolve(data.data)
          })
          .catch(function (e) {
            return reject(e)
          })
      })
    },

    post: (endpoint, data = {}) => {
      return new Promise((resolve, reject) => {
        axios
          .post(`${basePath}${endpoint}`, data)
          .then(function (data) {
            return resolve(data.data)
          })
          .catch(function (e) {
            return reject(e)
          })
      })
    },

    put: (endpoint, data = {}) => {
      return new Promise((resolve, reject) => {
        axios
          .put(`${basePath}${endpoint}`, data)
          .then(function (data) {
            return resolve(data.data)
          })
          .catch(function (e) {
            return reject(e)
          })
      })
    },
  }

  inject('api', api)
}
