export default (context) => () => ({
  get(code) {
    const Api = context.app.$api
    return Api.get(`/public/tickets/${code}`)
  },
  changeState(code, state, info) {
    const Api = context.app.$api
    return Api.put(`/admin/tickets/${code}/circulation/${state}`, {
      info,
    })
  },
})
