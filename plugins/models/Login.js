export default (context) => () => ({
  post(login, password) {
    const Api = context.app.$api
    return Api.post(`/public/login`, {
      login,
      password,
    })
  },
})
