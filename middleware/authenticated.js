export default function ({ route, redirect }) {
  if (!localStorage.token) {
    return redirect('/login')
  }

  if (route.params && route.params.role) {
    if (route.params.role !== localStorage.role) {
      return redirect('/' + localStorage.role)
    }
  } else if (
    route.matched &&
    route.matched[0] &&
    route.matched[0].props &&
    route.matched[0].props.default &&
    route.matched[0].props.default.role
  ) {
    if (route.matched[0].props.default.role !== localStorage.role) {
      return redirect('/' + localStorage.role)
    }
  } else {
    return redirect('/' + localStorage.role)
  }
}
