import Tickets from './models/Tickets.js'
import Prestataires from './models/Prestataires.js'
import Login from './models/Login.js'
import Logs from './models/Logs.js'

export default (context, inject) => {
  const models = {
    Tickets: Tickets(context),
    Prestataires: Prestataires(context),
    Login: Login(context),
    Logs: Logs(context),
  }

  inject('models', models)
}
