const qrcode = {}
let html5QrCodeInstance = null
// let cameraId = null;
qrcode.askCamera = function () {
  return new Promise((resolve, reject) => {
    // resolve(0)
    try {
      window.Html5Qrcode.getCameras()
        .then((devices) => {
          let backNbr = 0
          if (devices && devices.length) {
            for (let i = 0; i < devices.length; i++) {
              if (
                devices[i].label.includes('ack') ||
                devices[i].label.includes('rri')
              ) {
                backNbr++
              }
            }
            if (backNbr === 0) {
              return resolve(devices[0])
            } else {
              return resolve(null)
            }
          }
          reject(new Error('no mediaDevices'))
        })
        .catch((err) => {
          reject(err)
        })
    } catch (err) {
      reject(new Error('no mediaDevices'))
    }
  })
}

qrcode.startScan = function (callback, elementId, device = null) {
  const config = { aspectRatio: 1.0 }
  html5QrCodeInstance = new window.Html5Qrcode(elementId)
  if (device) {
    html5QrCodeInstance.start(device.id, config, callback)
  } else {
    html5QrCodeInstance.start(
      { facingMode: { exact: 'environment' } },
      config,
      callback
    )
  }
}

qrcode.stopScan = function () {
  html5QrCodeInstance
    .stop()
    .then((ignore) => {})
    .catch((err) => {
      throw err
    })
}

export default (context, inject) => {
  inject('qrcode', qrcode)
}
